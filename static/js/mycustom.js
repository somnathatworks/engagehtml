$(document).ready(function() {
	
	$(window).load(function() {		
		 $('.menu-overlay-links ul li').each(function(i){
			var t = $(this);
			setTimeout(function(){ 
			t.removeClass('animation'); 
			t.css({"transform": "translate(-10%, 0%) matrix(1, 0, 0, 1, 0, 0)","opacity": "0"});
			}, (i+1) * 200);
		});
		$( ".icon-share" ).trigger( "click" ); 
	});
	
	$(window).scroll(function() {   
		// alert('hi');
		var scroll = $(window).scrollTop();

		if (scroll >= 10) {
			$(".navigation").addClass("on");
			$(".navigation").removeClass("off");
		} else {
			$(".navigation").removeClass("on");
			$(".navigation").addClass("off");
		}
		
		$('.sharebox .Popover').removeClass("Popover-active");
		
		/* $('.menu-overlay-menu').removeClass('menu_active');
		 $('.panels-container.snapped').removeClass('panels_active');
		 $('.panels-container.snapped').animate({"transform": "translate(0%, 0%)"});
		 $('.menu-overlay2 .menu-dropback').css({"width": "0"});*/

    	    		 
		 
		
	});
	
	$('.header-navigtaion-left a').click(function(){
	
		 $('.panels-container.snapped').animate({"transform": "translate(20%, 0%)"});
		 $('.menu-overlay-menu').addClass('menu_active');
		 $('.panels-container.snapped').addClass('panels_active');
		 $('.menu-overlay2 .menu-dropback').css({"width": "100%"});	 
		 
		 $('.menu-overlay-links ul li').each(function(i){
			var t = $(this);
			setTimeout(function(){
			t.addClass('animation'); 
			t.css({"transform": "translate(0%, 0%) matrix(1, 0, 0, 1, 0, 0)","opacity": "1"});
			}, (i+1) * 150);
		});
		
	});
	
	$('.view-close').click(function(){
		 $('.menu-overlay-menu').removeClass('menu_active');
		 $('.panels-container.snapped').removeClass('panels_active');
		 $('.panels-container.snapped').animate({"transform": "translate(0%, 0%)"});
		 $('.menu-overlay2 .menu-dropback').css({"width": "0"});
		 
		 $('.menu-overlay-links ul li').each(function(i){
			var t = $(this);
			setTimeout(function(){ 
			t.removeClass('animation'); 
			t.css({"transform": "translate(-10%, 0%) matrix(1, 0, 0, 1, 0, 0)","opacity": "0"});
			}, (i+1) * 150);
		});
	});
	
	
	$('.icon-logo-ef').click(function(){
		 $('.panels-container.snapped').animate({"transform": "translate(20%, 0%)"});
		 $('.menu-overlay-flux').addClass('contact_active');
		 $('.panels-container.snapped').addClass('panels_active_right');
		 $('.menu-overlay-flux .menu-dropback').css({"width": "100%","left": "auto","right":"0px"});
	});
	
	
	$('.flux-dialog-close').click(function(){
		 $('.menu-overlay-flux').removeClass('contact_active');
		 $('.panels-container.snapped').removeClass('panels_active_right');
		 $('.panels-container.snapped').animate({"transform": "translate(0%, 0%)"});
		 $('.menu-overlay-flux .menu-dropback').css({"width": "0"});
	});
	
	
	$('.contactLink').click(function(){
		$('.panels-container.snapped').animate({"transform": "translate(20%, 0%)"});
		 $('.menu-overlay-flux').addClass('contact_active');
		 $('.panels-container.snapped').addClass('panels_active_right');
		 $('.menu-overlay-flux .menu-dropback').css({"width": "100%","left": "auto","right":"0px"});
		 
		 $('.menu-overlay-menu').removeClass('menu_active');
		 $('.panels-container.snapped').removeClass('panels_active');
		 $('.panels-container.snapped').animate({"transform": "translate(0%, 0%)"});
		 $('.menu-overlay2 .menu-dropback').css({"width": "0"});
	});
	
	$('.icon-share').click(function(){
		$('.social-media-selector .social-flex').css("display","flex");		
		$( ".social-accounts-list" ).toggle(function() {			  
			  $(this).css({"opacity": "1"}); 
			  $('.social-overlay').css({"width": "100%"});
			  $('.social-accounts-list a').css({"width": "100%","opacity":"1"});			  
			});
	});

    $('.icon-sharewidget').on('click', function(){
          $('.header-text-inner .Popover').toggleClass("Popover-active");
          $('.sharebox .Popover').toggleClass("Popover-active");		  
          //$('.header-text-inner .Popover').slideToggle();		  
	});

	$(".anchor a[href^='#'],.header-anchors a[href^='#'],.scroolid a[href^='#").click(function(e) {		
		e.preventDefault();
		var position = $($(this).attr("href")).offset().top;
		$("body, html").animate({
			scrollTop: position
		},500 );
	}); 
	

	/*$(".tab-links ul li").click(function() {		
		$(".tab-links ul li").removeClass('active');
		$(this).addClass('active');
	});*/
	$(".tab-links ul li.tab-content-0").click(function() {		
		$('#tab-content-1').css("display","none");			
		$('#tab-content-2').css("display","none");
		$('#tab-content-3').css("display","none");
		$('#tab-content-4').css("display","none");
		
		$('#tab-content-0').css("display","block");	
		$('.active-line').css("top","calc(0%)");		
	});
	
	$(".tab-links ul li.tab-content-1").click(function() {		
		
        $('#tab-content-0').css("display","none");
		$('#tab-content-2').css("display","none");
		$('#tab-content-3').css("display","none");
		$('#tab-content-4').css("display","none");
		
		$('#tab-content-1').css("display","block");	
        $('.active-line').css("top","calc(20%)");		
	});
	$(".tab-links ul li.tab-content-2").click(function() {		
		
        $('#tab-content-0').css("display","none");
		$('#tab-content-1').css("display","none");
		$('#tab-content-3').css("display","none");
		$('#tab-content-4').css("display","none");		
		
		$('#tab-content-2').css("display","block");
        $('.active-line').css("top","calc(40%)");		
	});
	$(".tab-links ul li.tab-content-3").click(function() {		
		
        $('#tab-content-0').css("display","none");	
		$('#tab-content-1').css("display","none");
		$('#tab-content-2').css("display","none");
		$('#tab-content-4').css("display","none");
		
		$('#tab-content-3').css("display","block");	
        $('.active-line').css("top","calc(60%)"); 		
	});
	$(".tab-links ul li.tab-content-4").click(function() {		
		
        $('#tab-content-0').css("display","none");
		$('#tab-content-1').css("display","none");
		$('#tab-content-2').css("display","none");
		$('#tab-content-3').css("display","none");	
		
		$('#tab-content-4').css("display","block");	
        $('.active-line').css("top","calc(80%)"); 			
	});
	
	
	
	
	if ($(window).width() < 767) {
			   $('.responsive_select_row').removeClass('casestudy-archive-row');
			   $('.responsive_select_left').removeClass('casestudy-archive-left');
			   $('.responsive_select_right').removeClass('casestudy-archive-right');
			}
			else {
				$('.responsive_select_row').addClass('casestudy-archive-row');
		        $('.responsive_select_left').addClass('casestudy-archive-left');
		        $('.responsive_select_right').addClass('casestudy-archive-right');
			};
	
	$(window).on('resize', function() {
		
			if ($(window).width() < 767) {
				$('.responsive_select_row').removeClass('casestudy-archive-row');
		        $('.responsive_select_left').removeClass('casestudy-archive-left');
		        $('.responsive_select_right').removeClass('casestudy-archive-right');
			} else {
				$('.responsive_select_row').addClass('casestudy-archive-row');
		        $('.responsive_select_left').addClass('casestudy-archive-left');
		        $('.responsive_select_right').addClass('casestudy-archive-right');
			}
		
	});
	
	
	////////////////////////////
	
	 $("[data-media]").on("click", function(e) {
		e.preventDefault();
		var $this = $(this);
		var videoUrl = $this.attr("data-media");
		var popup = $this.attr("href");
		var $popupIframe = $(popup).find("iframe");    
		$popupIframe.attr("src", videoUrl);    
		$this.closest(".page").addClass("show-popup");
		
	});
	$(".popup").on("click", function(e) {
		e.preventDefault();
		e.stopPropagation();    
		$(".page").removeClass("show-popup");	     
		
		var $this = $(this);
		var videoUrl = '';
		var popup = $this.attr("href");
		var $popupIframe = $('#myvideo');    
		$popupIframe.attr("src", videoUrl);
	});

	$(".popup > iframe").on("click", function(e) {
		e.stopPropagation();
	});
	
	/////////////////////////
		var swiper = new Swiper('.swiper-container', {
		  navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		  },
		});
	
    /////////////////////////
	
	
	


	

});

$(window).load(function() {
		var s = skrollr.init(); 
		s.refresh($('.hsContent'));
		var s = skrollr.init({
			forceHeight: false
		}); 
	});
	
	
	
$(document).ready(function() {
  	  var owl = $('.owl-carousel');
  	  owl.owlCarousel({
  		items: 6,
  		loop: true,
  		margin: 20,
  		autoplay: true,
  		autoplayTimeout:4000,
  		autoplaySpeed: 800,
  		//smartSpeed: 2500,
  		animateOut: 'fadeOut',
  		autoplayHoverPause: true,
  		responsiveClass:true,
  			responsive:{
  				0:{
  					items:3,
  					nav:true
  				},
  				600:{
  					items:4,
  					nav:false
  				},
  				1000:{
  					items:6,
  					nav:true,
  					loop:true
  				}
  			}
  	  });
  	  $('.play').on('click', function() {
  		owl.trigger('play.owl.autoplay', [1000])
  	  })
  	  $('.stop').on('click', function() {
  		owl.trigger('stop.owl.autoplay')
  	  })
  	});

$('.single-item').slick({
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 6000,
  speed: 1000,
  pauseOnHover:false,
  cssEase: 'ease-in-out'
});

$('.quote-container').mousedown(function(){
  $('.single-item').addClass('dragging');
});
$('.quote-container').mouseup(function(){
  $('.single-item').removeClass('dragging');
});

$(".scroolid a[href^='#']").click(function(e) {
	e.preventDefault();
	
	var position = $($(this).attr("href")).offset().top;

	$("body, html").animate({
		scrollTop: position
	},500 );
});

$('.click-to-more a,.modal-toggle').on('click', function(e) { 
  e.preventDefault();
  $('.modal').toggleClass('is-visible');
});
$('.getinTouch a,.modal-toggle').on('click', function(e) { 
  e.preventDefault();
  $('.modal_mail').toggleClass('is-visible');
});	
	


	
$(window).scroll(function() {
		    $('#testimonialSlider').each( function(i){
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
				var bottom_of_window = $(window).scrollTop() + $(window).height();							
				if( bottom_of_window + (bottom_of_window%10) > bottom_of_object ){
				     $("#sliderMovement").stop();
					 $('#sliderMovement').animate({'left':'0'}, 300);
				}else{
				     $("#sliderMovement").stop();
				     $('#sliderMovement').animate({'left':'-100%'}, 300);
				}
			});
			$('#testimonialSlider1').each( function(i){
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
				var bottom_of_window = $(window).scrollTop() + $(window).height();							
				if( bottom_of_window + (bottom_of_window%10) > bottom_of_object ){
				     $("#sliderMovement1").stop();
					 $('#sliderMovement1').animate({'left':'0'}, 300);
				}else{
				     $("#sliderMovement1").stop();
				     $('#sliderMovement1').animate({'left':'-100%'}, 300);
				}
			});
           $('#testimonialSlider2').each( function(i){
             	var bottom_of_object = $(this).offset().top + $(this).outerHeight();
				var bottom_of_window = $(window).scrollTop() + $(window).height();							
				if( bottom_of_window + (bottom_of_window%10) > bottom_of_object ){				     
					 $("#sliderMovement2").stop();
					 $('#sliderMovement2').animate({'left':'0'}, 300);	
				}else{
				     $("#sliderMovement2").stop();
				     $('#sliderMovement2').animate({'left':'-100%'}, 300);
				}				
			}); 
             $('#testimonialSlider3').each( function(i){
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
				var bottom_of_window = $(window).scrollTop() + $(window).height();							
				if( bottom_of_window + (bottom_of_window%10) > bottom_of_object ){				   
					 $("#sliderMovement3").stop();
					 $('#sliderMovement3').animate({'left':'0'}, 300);	
				}else{
				     $("#sliderMovement3").stop();
				     $('#sliderMovement3').animate({'left':'-100%'}, 300);
				}
			}); 
		});
		
		
$(window).load(function() {
  windowsize = $(window).width();
  if (windowsize > 1024) {
  
   
     ( function( $ ) {
			// Init Skrollr
			var s = skrollr.init();
			$window = $(window);
			$body = $('body');
			function adjustWindow(){
				winH = $window.height();
				if(winH <= 550) {
					winH = 550;
				} 
				$slide.height(winH);
			}
		} )( jQuery );
		
   
  }
});

$( function() {

    $('#masonrycontainer').masonry({
        itemSelector: '.masonry-grid-item',
       // columnWidth:300
    });

});